Validation PHP
================

Objectifs 
---------
* Comprendre et impémenter le POST-REDIRECT-GET
* Se familiariser avec l'outil Bootstrap (version 3.3)

Exercices
---------
La Saint-Valentin s'approche à grand pas. En bon samaritain, vous voulez aider les personnes célibataires à trouver l'âme soeur. Pour ce faire, vous décidez de créer un formulaire de profil personnel. 

1. Créez un formulaire avec les champs suivants: 
    * Nom
    * Prénom 
    * Numéro de téléphone
    * Âge
    * Description (sous forme de textarea)
    * Un bouton soumettre 
  
    De plus, votre formulaire doit être envoyé avec la méthode POST et doit implémenter le POST-REDIRECT-GET.  

2. Validez les données côté serveur (en PHP) afin que le formulaire respecte les règles suivantes:  
    * Tous les champs sont obligatoires 
    * L'âge doit être un chiffre entre 18 et 99 
    * Le numéro de téléphone doit respecter le format 111-111-1111
  
    La validation doit être séparée du fichier contenant le formulaire.  

    Si l'utilisateur envoie un formulaire invalide, un message d'erreur s'affiche près du champ problémantique. Les données entrées par l'utilisateur sont conservées. Si le formulaire est valide, un message de succès est affiché sur la page du formulaire. 

3. Améliorez le visuel de la page à l'aide de Bootstrap et CSS.
