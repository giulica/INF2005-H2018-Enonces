Formulaires HTML
================

Exercices
---------

1. Créez une page de commande de pizza en ligne. La page doit contenir un titre et un formulaire. Le formulaire doit avoir les champs suivants: 
   * Nom 
   * Prénom 
   * Adresse
   * Code postal 
   * Numéro de téléphone 
   * Le choix de pizza 
   * Allergies
   * Un boutton 'Soumettre' 

    Choisissez le type de champs qui vous semble plus approprié.  
    Utilisez un formulaire avec la méthode POST.  

2. Validez les données côté serveur (en PHP) afin que le formulaire respecte les règles suivantes: 
   * Les champs nom, prénom, adresse, code postal, numéro de téléphone et le choix de pizza sont obligatoires 
   * Le code postal doit respecter le format X1X 1Y1 
   * Le numéro de téléphone doit respecter le format 111-111-1111 

    Si le formulaire est invalide, un message d'erreur indiquant le champ erroné est affiché. Les champs entrés par l'utilisateur ne doivent pas être perdus.    
    Si le formulaire est valide, un message de confirmation est affiché.  

3. Améliorez le visuel de la page à l'aide de Bootstrap et CSS. 
